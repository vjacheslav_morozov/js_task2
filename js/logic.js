var inputTextElement;
var taskListElement;
var allViewTypeRadiobutton;
var activeViewTypeRadiobutton;
var completedViewTypeRadiobutton;
var dateSortTypeRadiobutton;
var a_zSortTypeRadiobutton;
var z_aSortTypeRadiobutton;
var randomSortTypeRadiobutton;
var taskNumber;
var viewType = "all";
var sortType = "date";
var taskList = [];
var storage = window.localStorage;
/*
 * Model
 */

// Initialization
var init = function() {
	inputTextElement = document.getElementById("input_task");
	inputTextElement.addEventListener("keypress", function(e) {
		onInputTextKeyPress(e);
	});
	inputTextElement.placeholder = "   What needs to be done?   ";

	allViewTypeRadiobutton = document.getElementById("all_radiobutton");
	allViewTypeRadiobutton.addEventListener("click", function(e) {
		onViewTypeRadiobuttonClicked("all");
		changeChekcedViewTypeRadiobutton(allViewTypeRadiobutton);
	});

	activeViewTypeRadiobutton = document.getElementById("active_radiobutton");
	activeViewTypeRadiobutton.addEventListener("click", function(e) {
		onViewTypeRadiobuttonClicked("active");
		changeChekcedViewTypeRadiobutton(activeViewTypeRadiobutton);
	});

	completedViewTypeRadiobutton = document
			.getElementById("completed_radiobutton");
	completedViewTypeRadiobutton.addEventListener("click", function(e) {
		onViewTypeRadiobuttonClicked("completed");
		changeChekcedViewTypeRadiobutton(completedViewTypeRadiobutton);
	});

	dateSortTypeRadiobutton = document.getElementById("date_sort_radiobutton");
	dateSortTypeRadiobutton.addEventListener("click", function(e) {
		onSortTypeRadiobuttonClicked("date");
		changeChekcedSortTypeRadiobutton(dateSortTypeRadiobutton);
	});

	a_zSortTypeRadiobutton = document.getElementById("a-z_sort_radiobutton");
	a_zSortTypeRadiobutton.addEventListener("click", function(e) {
		onSortTypeRadiobuttonClicked("a-z");
		changeChekcedSortTypeRadiobutton(a_zSortTypeRadiobutton);
	});

	z_aSortTypeRadiobutton = document.getElementById("z-a_sort_radiobutton");
	z_aSortTypeRadiobutton.addEventListener("click", function(e) {
		onSortTypeRadiobuttonClicked("z-a");
		changeChekcedSortTypeRadiobutton(z_aSortTypeRadiobutton);
	});

	randomSortTypeRadiobutton = document
			.getElementById("random_sort_radiobutton");
	randomSortTypeRadiobutton.addEventListener("click", function(e) {
		onSortTypeRadiobuttonClicked("random");
		changeChekcedSortTypeRadiobutton(randomSortTypeRadiobutton);
	});

	window.onbeforeunload = function() {
		saveTasksToStorage();
	};

	taskListElement = document.getElementById("task_conteiner");

	loadTasksFromStorage();
	sortTaskList();
	repaintTaskList();
};

// Work with storage

var loadTasksFromStorage = function() {
	taskNumber = storage["taskNumber"] * 1;
	if (!taskNumber) {
		taskNumber = 0;
	}
	for ( var taskId in storage) {
		if (taskId == "taskNumber") {
			continue;
		}
		var taskString = storage[taskId];
		var delimiterPoz = taskString.indexOf(";");
		var task = {};
		task.id = taskId;
		if (taskString.substring(0, delimiterPoz) == "false") {
			task.isComplited = false;
		} else {
			task.isComplited = true;
		}
		task.content = taskString.substring(delimiterPoz + 1);
		taskList.push(task);
	}
};

var saveTasksToStorage = function() {
	clearStorage();
	storage["taskNumber"] = taskNumber;
	for ( var taskId in taskList) {
		var task = taskList[taskId];
		storage[task.id] = task.isComplited + ";" + task.content;
	}
};

var clearStorage = function() {
	for ( var taskId in storage) {
		delete storage[taskId];
	}
};
// Listeners

var onInputTextKeyPress = function(event) {
	if (event.which == 13 || event.keyCode == 13) {
		var taskContent = inputTextElement.value;
		if (taskContent) {
			inputTextElement.value = "";
			var taskId = "task_" + taskNumber;
			taskNumber++;
			var task = {};
			task.id = taskId;
			task.isComplited = false;
			task.content = taskContent;
			taskList.push(task);
			sortTaskList();
			repaintTaskList();
		}
	}
};

var onCheckedTask = function(task) {
	task.isComplited = !task.isComplited;
	repaintTaskList();
};

var onRemoveTask = function(task) {
	for ( var index in taskList) {
		currentTask = taskList[index];
		if (currentTask.id === task.id) {
			delete taskList[index];
			repaintTaskList();
			break;
		}
	}
};

var onViewTypeRadiobuttonClicked = function(type) {
	viewType = type;
	repaintTaskList();
};

var onSortTypeRadiobuttonClicked = function(type) {
	sortType = type;
	sortTaskList();
	repaintTaskList();
};

// Comparators

var dateComparator = function(task1, task2) {
	return compareStrings(task1.id, task2.id);
};

var a_zComparator = function(task1, task2) {
	return compareStrings(task1.content, task2.content);
};

var z_aComparator = function(task1, task2) {
	return -compareStrings(task1.content, task2.content);
};

var randomComparator = function(task1, task2) {
	return (Math.round(Math.random() * 2) - 1);
};

// sorting

var sortTaskList = function() {
	var comporator = null;
	if ("date" == sortType) {
		comporator = dateComparator;
	} else if ("a-z" == sortType) {
		comporator = a_zComparator;
	} else if ("z-a" == sortType) {
		comporator = z_aComparator;
	} else if ("random" == sortType) {
		comporator = randomComparator;
	}
	taskList.sort(comporator);
};

var compareStrings = function(string1, string2) {
	if (string1 > string2) {
		return 1;
	}
	if (string1 < string2) {
		return -1;
	}
	return 0;
};

/*
 * View
 */

var repaintTaskList = function() {
	clearTaskList();
	for ( var index in taskList) {
		task = taskList[index];
		if ("all" == viewType) {
			taskElementAppend(task);
		} else if (("active" == viewType) && (!task.isComplited)) {
			taskElementAppend(task);
		} else if (("completed" == viewType) && (task.isComplited)) {
			taskElementAppend(task);
		}
	}
};

var clearTaskList = function() {
	var children = taskListElement.childNodes;
	while (children.length) {
		taskListElement.removeChild(children[0]);
	}
};

var taskElementAppend = function(task) {

	var taskClid = document.createElement("div");
	var taskClass;
	if (task.isComplited) {
		taskClass = "complite_todo_string";
	} else {
		taskClass = "todo_string";
	}
	taskClid.setAttribute("class", taskClass);

	var checkboxClid = document.createElement("div");
	checkboxClid.setAttribute("class", "checkbox");
	checkboxClid.addEventListener("click", function(e) {
		onCheckedTask(task);
	});

	var taskContentClid = document.createElement("div");
	taskContentClid.setAttribute("class", "task_content");
	taskContentClid.innerHTML = task.content;

	var deleteTaskClid = document.createElement("div");
	deleteTaskClid.setAttribute("class", "remove_task");
	deleteTaskClid.addEventListener("click", function(e) {
		onRemoveTask(task);
	});
	taskClid.appendChild(checkboxClid);
	taskClid.appendChild(deleteTaskClid);
	taskClid.appendChild(taskContentClid);
	taskListElement.appendChild(taskClid);
};

var changeChekcedViewTypeRadiobutton = function(checkedViewTypeRadiobutton) {
	allViewTypeRadiobutton.setAttribute("class", "radiobutton");
	activeViewTypeRadiobutton.setAttribute("class", "radiobutton");
	completedViewTypeRadiobutton.setAttribute("class", "radiobutton");
	checkedViewTypeRadiobutton.setAttribute("class", "checked_radiobutton");
};

var changeChekcedSortTypeRadiobutton = function(checkedSortTypeRadiobutton) {

	dateSortTypeRadiobutton.setAttribute("class", "sort_radiobutton");
	a_zSortTypeRadiobutton.setAttribute("class", "sort_radiobutton");
	z_aSortTypeRadiobutton.setAttribute("class", "sort_radiobutton");
	randomSortTypeRadiobutton.setAttribute("class", "sort_radiobutton");
	checkedSortTypeRadiobutton
			.setAttribute("class", "checked_sort_radiobutton");
};